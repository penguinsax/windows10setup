# Windows10 Install 
1. Install virtio-win tools
2. Install Spice-tools
3. Enable RDP Setting->System->Remote Desktop
    - Enable remote desktop
    - Advanced-> Do not "Require Network Level Authentication"
4. Add password
5. change User Account Control Settings down one notch
6. Change Hostname Setting->System->About
7. shutdown, 
8. switch to spice display
9. Boot
10. disable sleep

## Install Chocolatey
#### from Powershell (Admin)
```
Set-ExecutionPolicy AllSigned
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

#### Restart Powershell
## Install Base Packages
```
choco install 7zip brave firefox mremoteng notepadplusplus putty veracrypt vivaldi vlc windirstat winscp -y
```

###### Installed
  - 7zip
  - brave
  - firefox
  - mremoteng
  - notepadplusplus
  - putty
  - veracrypt
  - vivaldi
  - vlc
  - windirstat
  - winscp


## Install Office Packs
`choco install libreoffice-fresh office365business -y`

  - libreoffice-fresh
  - office365business
#  - visio
#  - teams
  
## Install Extra Packages
```
choco install discord mpc-hc moonlight-qt nextcloud-client parsec steam syncthing thunderbird tightvnc x2go -y
```
  - discord
  - nextcloud-client
  - parsec
  - steam
  - moonlight-qt
  - mpc-hc #Windows Media Player
  - syncthing
  - tightvnc
  - x2go client (for server with gui)

## Other Packages
  - barrier
  - dropbox
  - epicgameslauncher
  - gnucash
  - virtualbox
  - openvpn
  - open-shell
  - thunderbird
  - veyon
  - xplorer2
  - kmymoney.portable
  - md5sums
  - adobereader

  
## Turn on/off hibernation
    `powercfg.exe /hibernate off`
#### off
    `powercfg.exe /hibernate on`

## Add MD5Sum to right click
https://www.wikihow.com/Add-New-Options-to-Right-Click-Menu-in-Windows
  1. navigate to `C:\Users\jmelland\AppData\Roaming\Microsoft\Windows\SendTo`
  2. Create shortcut `md5sums -p`
  

## Reference Locations
C:\ProgramData\Microsoft\Windows\Start Menu
C:\Users\jmelland\AppData\Roaming\Microsoft\Windows\SendTo

